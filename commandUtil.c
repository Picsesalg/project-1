/*
  Midterm Project
  commmandUtil.c
  Alice Yang
  Minqi Ma
  ayang36 mma17
 */

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "commandUtil.h"

int crop_isNumber(char *argv[]) {
  for (int i = 4; i < 8; i++) {
    if (!isdigit(*argv[i])) {
      return i;
    }
  }
  return 0;
}

int crop_validNum(char *argv[], int rows, int cols) {
  int x1 = atoi(argv[4]);
  int y1 = atoi(argv[5]);
  int x2 = atoi(argv[6]);
  int y2 = atoi(argv[7]);

  if (x1 < 0 || x1 > cols - 1) {
    return 1;
  }
  else if (y1 < 0 || y1 > rows - 1) {
    return 1;
  }
  else if (x2 < x1 + 1 || x2 > cols) {
    return 1;
  }
  else if (y2 < y1 + 1 || y2 > rows) {
    return 1;
  }

  return 0;
}


FILE *opening (char *file_name) {
  FILE *to_read = fopen(file_name, "rb");
  if (!to_read) {
    return NULL;
  }
  else {
    return to_read;
  }
}

int choice(char *input_op) {
  if (strcmp(input_op, "swap") == 0) {
    return 1;
  }
  else if (strcmp(input_op, "blackout") == 0) {
    return 2;
  }
  else if (strcmp(input_op, "crop") == 0) {
    return 3;
  }
  else if (strcmp(input_op, "grayscale") == 0) {
    return 4;
  }
  else if (strcmp(input_op, "contrast") == 0) {
    return 5;
  }
  else {
    return 6;
  }
}
