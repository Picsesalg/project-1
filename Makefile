# Midterm project 1
# Makefile
# Alice Yang
# Minqi Ma
# ayang36
# mma17

#
# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CONSERVATIVE_FLAGS = -std=c99 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

# Links the project executable
project: project.o ppmIO.o imageManip.o commandUtil.o
	$(CC) -o project project.o ppmIO.o imageManip.o commandUtil.o

# Compiles project.c into an object file
project.o: project.c ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c project.c

# Compiles ppmIO.c into an object file
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

# Compiles imageManip.c into an object file
imageManip.o: imageManip.c imageManip.h ppmIO.h
	$(CC) $(CFLAGS) -c imageManip.c

# Compiles commandUtil.o into an object file
commandUtil.o: commandUtil.c commandUtil.h
	$(CC) $(CFLAGS) -c commandUtil.c

# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o project ppmIO *.gcov
