/*
  Midterm Project 1
   ppmIO.c
   Minqi Ma
   Alice Yang
   mma17 ayang36
*/

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"



/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {
  
  int rows = 0;
  int cols = 0;
  int colors = 0;
  int temp = 0;  //store the value of read character
  int count = 0;  //the number of necessary tags in the header
  int return_value;  //store return value of fgetc/fscanf

  while (count < 4) {  //read until there are four tags
    temp = fgetc(fp);
    if (temp == EOF) { //if reach EOF before reading 4 tags
      return NULL;
    }  
    if(temp == ' ' || temp == '\n' || temp == '\t') {  //skip white space
    }
    else if (temp == '#') {  //skip comment starting with #
      do {
	temp = fgetc(fp);
	if (temp == EOF) {
	  return NULL;
	}
      } while (temp != '\n');
    }
    else {
      count++;
      return_value = ungetc(temp, fp);
      if (return_value == EOF) {
	return NULL;
      }  //error if ungetc fails

      switch (count) {
	
	//check if first tag is P6
      case 1:
	temp = fgetc(fp);
	if (temp != 'P') {
	  return NULL;
	}
	temp = fgetc(fp);
	if (temp != '6') {
	  return NULL;
	}
	fgetc(fp);  //skip new line chracter
	break;

	//read cols from second tag
      case 2:
	return_value = fscanf(fp, "%d", &cols);
	if (return_value != 1) {return NULL;} //error if fscanf fails
	fgetc(fp);
	break;

	//read rows from third tag
      case 3:
	return_value = fscanf(fp, "%d", &rows);
	if (return_value != 1) {return NULL;}  //error if fscanf fails
	fgetc(fp);
	break;

	//check if fourth tag is 255
      case 4:
	return_value = fscanf(fp, "%d", &colors);
	if (return_value != 1) {return NULL;}
	if (colors != 255) {return NULL;}
	fgetc(fp);
	break;
      }
     }
    }

  Image* PPM_read = malloc(sizeof(Image));
  PPM_read->data= malloc(rows*cols*sizeof(Pixel));
  PPM_read->rows = rows;
  PPM_read->cols = cols;
  return_value = fread(PPM_read->data, sizeof(Pixel), rows*cols, fp);  
  if (return_value != rows * cols) {return NULL;}  //if there's not enough data in the image
  return PPM_read; 
}




/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }

  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  /* write pixels */
  int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    return -1;
  }

  /* success, so return number of pixels written */
  return num_pixels_written;
}



