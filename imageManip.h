/*
  Midterm Project
  imageManip.h
  Alice Yang
  Minqi Ma
  ayang36 mma17
 */

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H
#include "ppmIO.h"

/*
  Swaps the values of red, green, blue channels
  *PPM_read is the pointer to the image to be read
 */
void swap(Image *PPM_read);

/*
  Blacks out the top right quadrant of the image
  *PPM_read is the pointer to the image to be read
 */
void blackout(Image *PPM_read);

/*
  Crops the image to the desired dimensions
  *PPM_read the image to be altered
  a is the top row
  b is the most left column
  c is the bottom row
  d is the most right column
  Returns pointer to the cropped image.
 */
Image* crop(Image* PPM_read, int a, int b, int c, int d);

/*
  Converts image to grayscale
  *PPM_read is the image to be altered
 */
void grayscale(Image *PPM_read);

/*
  Adds contrast to the image
  *PPM_read is the image to be altered
  factor the amount by which the image is being saturated
 */
void contrast(Image *PPM_read, double factor);

/*
  Converts the values of the pixel channels
  value is the original value of the pixel channel
  factor is the user input
  Returns the adjusted value
 */
int contrast_helper(int value, double factor);
#endif
