/*
  Midterm Project
  commmandUtil.h
  Alice Yang
  Minqi Ma
  ayang36 mma17
 */

#ifndef COMMANDUTIL_H
#define COMMANDUTIL_H
#include <stdio.h>

/* Checks if the arguments for crop are numbers
   argv the string user input
   Retuens 1 if not number
   Returns 0 if is number
 */
int crop_isNumber(char *argv[]);

/* Check if the inputted dimensions are valid or not
   argv is the input string
   rows is the number of rows
   cols is the number of columns
   Return 1 if there is any error
   Return 0 if the input is valid.
 */
int crop_validNum(char *argv[], int rows, int cols);

/* Open the file. 
   Return a file pointer to read from
   Return NULL if fails.
 */
FILE *opening(char *file_name);

/* Translates the user choice into a number.
   input_op is the user's inputted operation
   Return 1 for swap
   Return 2 for blackout
   Return 3 for crop
   Return 4 for grayscale
   Return 5 for contrast
   Return 6 if input_op is invalid
 */
int choice (char *input_op);

#endif
