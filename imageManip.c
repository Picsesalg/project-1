/* 
   Midterm Project
   imageManip.c
   Alice Yang
   Minqi Ma
   ayang36 mma17
 */

#include <stdlib.h>
#include "imageManip.h"
#include "ppmIO.h"

void swap(Image* PPM_read) {
  unsigned char temp = 0;
  for (int i = 0; i < PPM_read->rows * PPM_read->cols; i++) {
    temp = PPM_read->data[i].r; //Temporarily holds the value of red
    PPM_read->data[i].r = PPM_read->data[i].g;
    PPM_read->data[i].g = PPM_read->data[i].b;
    PPM_read->data[i].b = temp;
  }
}

void blackout (Image *PPM_read) {
  for (int i = 0; i < PPM_read->rows / 2; i++) {
    for (int j = ((PPM_read->cols + 1) / 2); j < PPM_read->cols; j++) {
      PPM_read->data[i * PPM_read->cols + j].g = 0;
      PPM_read->data[i * PPM_read->cols + j].r = 0;
      PPM_read->data[i * PPM_read->cols + j].b = 0;
    }
  }
}

Image* crop (Image* PPM_read, int a, int b, int c, int d) {
  Image* cropped = malloc(sizeof(Image)); //Holds the new cropped image
  if (!cropped) { //If malloc fails
    return NULL;
  }
  cropped->data = malloc((c - a) * (d - b) * sizeof(Pixel));
  cropped->rows = c - a;
  cropped->cols = d - b;
  //Loops through each pixel in the new image and copies from the old image
  for (int i = 0; i < (c - a); i++) {
    for (int j = 0; j < (d - b); j++) {
      cropped->data[i * (d - b) + j].r = PPM_read->data[(i + a) * PPM_read->cols + (j + b)].r;
      cropped->data[i * (d - b) + j].g = PPM_read->data[(i + a) * PPM_read->cols + (j + b)].g;
      cropped->data[i * (d - b) + j].b = PPM_read->data[(i + a) * PPM_read->cols + (j + b)].b;
    }
  }
  return cropped;
}

void grayscale (Image *PPM_read) {
  unsigned char intensity = 0;
  //Cycles through each image and changes the value each time
  for (int i = 0; i < PPM_read->rows * PPM_read->cols; i++) {
    intensity = (unsigned char)(0.3 * PPM_read->data[i].r + 0.59 * PPM_read->data[i].g + 0.11 * PPM_read->data[i].b);
    PPM_read->data[i].r = intensity;
    PPM_read->data[i].g = intensity;
    PPM_read->data[i].b = intensity;
  }
}

void contrast(Image* PPM_read, double factor) {
  for (int i = 0; i < PPM_read->rows * PPM_read->cols; i++) {
    PPM_read->data[i].r = contrast_helper(PPM_read->data[i].r, factor);
    PPM_read->data[i].g = contrast_helper(PPM_read->data[i].g, factor);
    PPM_read->data[i].b = contrast_helper(PPM_read->data[i].b, factor);  
  }
}

int contrast_helper(int value, double factor) {
  //Converts value into the range around [-0.5, 0.5] and multiplies by the factor
  double new_value = (1.0 / 255 * value - 0.5) * factor;
  //Converts back into [0, 255]
  int convert_value = (int) (255 * new_value + 255 / (double) 2);
  if (convert_value > 255) {
    return 255;
  }
  else if (convert_value < 0) {
    return 0;
  }
  else {
    return convert_value;
  }
}
