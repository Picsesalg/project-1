/* Midterm Project 1
   project.c
   Alice Yang
   Minqi Ma
   ayang36
   mma17
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"

/* Main program
   argc is whether or not we have a file name
   *argv[] is the user input in the format: input file, output file, function, necessary input arguments for different output
 */
int main(int argc, char *argv[]) {

  //The input file we'll be reading from
  FILE* to_read = NULL;
  //The output file we'll be writing to
  FILE* to_write = NULL;

  if (argc < 3) {  //if no input/output file
    fprintf(stderr, "[ERROR!] Input and output file expected.\n");
    return 1;
  }
  else {
    to_read = opening(argv[1]);
    if (to_read == NULL) {
      fprintf(stderr, "[ERROR!] %s can't be opened.\n", argv[1]);
      return 2;
    }
  }

  //If the PPM is formatted incorrectly
  //Reads the .ppm file
  Image *PPM_read = readPPM(to_read);
  if (PPM_read == NULL) {
    printf("[ERROR!] The input file is not formatted correctly.\n");
    return 3;
  }

  if (argc < 4) {
    fprintf(stderr, "[ERROR!] No operation name specified.\n");
    return 5;
  }
  
  //User's choice will be translated to a number.
  int operation = choice(argv[3]);

  switch(operation) {

    //swap
  case 1:
    swap(PPM_read);
    break;

    //blackout
  case 2:
    blackout(PPM_read);
    break;

    //crop
  case 3:
    //check if there are enough arguments
    if (argc != 8) {
      fprintf(stderr, "[ERROR!] The dimensions for crop are missing.\n");
      return 6;
    }
    //check if input arguments are numbers
    else if (crop_isNumber(argv)) {
      fprintf(stderr, "[ERROR!] %s is not a number.\n", argv[crop_isNumber(argv)]);
      return 6;
    }
    //check if numbers are in the valid range
    else if (crop_validNum(argv, PPM_read->rows, PPM_read->cols)) {
      fprintf(stderr, "[ERROR!] The range is invalid.\n");
      return 7;
    }
    else {  
      Image *cropped = crop(PPM_read, atoi(argv[5]), atoi(argv[4]), atoi(argv[7]), atoi(argv[6]));
      if (cropped == NULL) {
	fprintf(stderr, "[ERROR!] Malloc for cropped failed.\n");
	return 8;
      }
      free(PPM_read->data);  //free old image
      free(PPM_read);
      PPM_read = cropped;
    }
    break;

    //grayscale
  case 4:
    grayscale(PPM_read);
    break;

    //contrast
  case 5:
    //check if there are enough arguments
    if (argc != 5) {
      fprintf(stderr, "[ERROR!] The adjustment factor is missing.\n");
      return 6;
    }
    //check if the argument is a number
    else if (!isdigit(*argv[4])){
      fprintf(stderr, "[ERROR!] %s is not a number.\n", argv[4]);
      return 6;
    }
    else {
      contrast(PPM_read, atof(argv[4]));
    }
    break;

    //invalid operation
  case 6:
    fprintf(stderr, "[ERROR!] %s is invalid.\n", argv[3]);
    return 5;
    break;
  }

  to_write = fopen(argv[2], "wb");
  if (to_write == NULL) {
    fprintf(stderr, "[ERROR]! %s cannot be opened for writing.\n", argv[2]);
    return 4;
  }
  int num_pixels = writePPM(to_write, PPM_read);  //write new image
  if (num_pixels != PPM_read->rows * PPM_read->cols) {
    fprintf(stderr, "[ERROR!] to_write doesn't work.");
    return 4;
  }

  //free images
  free(PPM_read->data);
  free(PPM_read);
  //close files
  fclose(to_read);
  fclose(to_write);

  return 0;
}
